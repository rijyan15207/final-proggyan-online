
package cart;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean(name = "obj5")
@SessionScoped
public class CartHandlerMB 
{
    List<CartBind> cartList = new ArrayList<CartBind>();
    Map<Integer, CartBind> map = new HashMap<Integer, CartBind>();
    
    int productid;
    String productname;
    int quantity;
    double price;
    double total;
    double allTotal = 0;
    int cartsize;
    String item = "item";
    String imgUrl;

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    
    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }
    
    public int getCartsize() {
        return cartsize;
    }

    public void setCartsize(int cartsize) {
        this.cartsize = cartsize;
    }
    
    public int getProductid() {
        return productid;
    }

    public void setProductid(int productid) {
        this.productid = productid;
    }

    public Map<Integer, CartBind> getMap() {
        return map;
    }

    public void setMap(Map<Integer, CartBind> map) {
        this.map = map;
    }

    public List<CartBind> getCartList() {
        return cartList;
    }

    public void setCartList(List<CartBind> cartList) {
        this.cartList = cartList;
    }

    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getAllTotal() {
        return allTotal;
    }

    public void setAllTotal(double allTotal) {
        this.allTotal = allTotal;
    }
    
    
    
    
    
    
    public String processCart(int qty)
    {
        CartBind cb = new CartBind();
        
        quantity = qty;
        cb.setProductname(productname);
            System.out.println(quantity + "----------------");
        cb.setQuantity(quantity);
        cb.setPrice(price);
        cb.setTotal(quantity * price);
        
        allTotal = allTotal + quantity * price;
        
        cartList.add(cb);
        map.put(productid, cb);
        cartsize = cartList.size();
        
        if(cartsize > 1)
            item = "items";
        return null;
    }
    
}
